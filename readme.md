# Tasks
* (_elementary_) Provide an implementation that passes all tests. Utilize the Stream API where possible.
* (_advanced_) Implement the `findLongestConnection` method that searches the first flight with the maximum number of stops.
