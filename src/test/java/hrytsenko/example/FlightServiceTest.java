package hrytsenko.example;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

public class FlightServiceTest {

    private FlightService service;

    @Before
    public void init() {
        service = new FlightService();
    }

    @Test
    public void noFlights() {
        List<Flight> flights = Collections.emptyList();

        Optional<Flight> flight = service.findShortestConnection("ANY", "ANY", flights);

        assertThat(toString(flight), is(equalTo("")));
    }

    @Test
    public void noConnections() {
        List<Flight> flights = Arrays.asList(Flight.nonStop("KBP", "AMS"), Flight.nonStop("VIE", "FRA"));

        Optional<Flight> flight = service.findShortestConnection("KBP", "FRA", flights);

        assertThat(toString(flight), is(equalTo("")));
    }

    @Test
    public void oneConnection_nonStop() {
        List<Flight> flights = Arrays.asList(Flight.nonStop("KBP", "FRA"));

        Optional<Flight> flight = service.findShortestConnection("KBP", "FRA", flights);

        assertThat(toString(flight), is(equalTo("KBP:FRA")));
    }

    @Test
    public void oneConnection_oneStop() {
        List<Flight> flights = Arrays.asList(Flight.nonStop("KBP", "AMS"), Flight.nonStop("AMS", "FRA"));

        Optional<Flight> flight = service.findShortestConnection("KBP", "FRA", flights);

        assertThat(toString(flight), is(equalTo("KBP:AMS:FRA")));
    }

    @Test
    public void twoWayConnections() {
        List<Flight> flights = Arrays.asList(Flight.nonStop("KBP", "AMS"), Flight.nonStop("AMS", "KBP"),
                Flight.nonStop("AMS", "FRA"), Flight.nonStop("FRA", "AMS"));

        Optional<Flight> flight = service.findShortestConnection("KBP", "FRA", flights);

        assertThat(toString(flight), is(equalTo("KBP:AMS:FRA")));
    }

    @Test
    public void severalConnections_equalLength_chooseFirst() {
        List<Flight> flights = Arrays.asList(Flight.nonStop("KBP", "AMS"), Flight.nonStop("AMS", "VIE"),
                Flight.nonStop("AMS", "FRA"), Flight.nonStop("VIE", "FRA"));

        Optional<Flight> flight = service.findShortestConnection("KBP", "FRA", flights);

        assertThat(toString(flight), is(equalTo("KBP:AMS:FRA")));
    }

    @Test
    public void severalConnections_differentLength_chooseShortest() {
        List<Flight> flights = Arrays.asList(Flight.nonStop("KBP", "AMS"), Flight.nonStop("AMS", "FRA"),
                Flight.nonStop("KBP", "FRA"));

        Optional<Flight> flight = service.findShortestConnection("KBP", "FRA", flights);

        assertThat(toString(flight), is(equalTo("KBP:FRA")));
    }

    private static String toString(Optional<Flight> flight) {
        return flight.map(FlightServiceTest::toString).orElse("");
    }

    private static String toString(Flight flight) {
        StringBuilder output = new StringBuilder();
        output.append(flight.getFrom()).append(":");
        if (flight.hasConnections()) {
            output.append(flight.getConnections().stream().map(Flight::getTo).collect(Collectors.joining(":")));
        } else {
            output.append(flight.getTo());
        }
        return output.toString();
    }

}
