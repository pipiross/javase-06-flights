package hrytsenko.example;

import java.util.List;
import java.util.Optional;

public class FlightService {

    /**
     * Finds the flight with minimum number of stops.
     * 
     * @param from
     *            the departure airport.
     * @param to
     *            the arrival airport.
     * @param flights
     *            the list of existing flights.
     * 
     * @return the flight with minimum number of stops or the first flight according to the flights order, if there are
     *         several flights with the same number of stops.
     */
    public Optional<Flight> findShortestConnection(String from, String to, List<Flight> flights) {
        throw new UnsupportedOperationException();
    }

}
