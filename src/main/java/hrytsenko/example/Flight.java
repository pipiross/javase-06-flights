package hrytsenko.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The flight between airports.
 * 
 * <p>
 * For non-stop flights, connections list is empty. Otherwise, connections list contains all connected flights.
 */
public class Flight {

    private final String from;
    private final String to;

    private final List<Flight> connections;

    /**
     * Create a flight.
     * 
     * @param from
     *            the departure airport.
     * @param to
     *            the arrival airport.
     * @param connections
     *            the connected flights (should be empty for the non-stop flight).
     */
    public Flight(String from, String to, List<Flight> connections) {
        this.from = from;
        this.to = to;
        this.connections = new ArrayList<>(connections);
    }

    /**
     * Create a non-stop flight.
     * 
     * @param from
     *            the departure airport.
     * @param to
     *            the arrival airport.
     * 
     * @return the non-stop flight.
     */
    public static Flight nonStop(String from, String to) {
        return new Flight(from, to, Collections.emptyList());
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public List<Flight> getConnections() {
        return connections;
    }

    public boolean hasConnections() {
        return !connections.isEmpty();
    }

}
